require 'rails_helper'

describe TeachersController, type: :controller do

  describe 'Index action' do
    it 'Techers value should contain two teachers' do
      create :teacher
      create :teacher, email: 'teacher@test.com'
      get :index

      expect(assigns(:teachers).length).to eq 2
    end
  end

end
