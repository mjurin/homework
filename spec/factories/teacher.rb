FactoryGirl.define do
  factory :teacher do
    name 'John Smith'
    email 'john@smith.com'
  end
end
