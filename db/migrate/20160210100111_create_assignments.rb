class CreateAssignments < ActiveRecord::Migration
  def change
    create_table :assignments do |t|
      t.integer :teacher_id
      t.integer :student_id
      t.string :content_link
      t.string :short_description
      t.timestamps null: false
    end
  end
end
