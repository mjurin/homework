class TeachersController < ApplicationController
  before_filter :set_teacher, only: [ :show]

  def index
    @teachers = Teacher.all
  end

  def show
    @assignments = @teacher.assignments
  end

  private
    def set_teacher
      @teacher = Teacher.find(params[:id])
    end

end
