class StudentsController < ApplicationController
  before_filter :set_student, only: [ :show]

  def index
    @students = Student.all
  end

  def show
    @assignments = @student.assignments
  end

  private
    def set_student
      @student = Student.find(params[:id])
    end

end
