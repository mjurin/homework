class CommentsController < ApplicationController
  def new
    @comment = Comment.new
    @comments = Comment.order('created_at DESC')
  end

  def create
    respond_to do |format|
      @comment = Comment.new(comment_params)
      if @comment.save
        flash.now[:success] = 'Your comment was successfully posted!'
      else
        flash.now[:error] = 'Your comment cannot be saved.'
      end
      format.html {redirect_to root_url}
      format.js
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:body, :assignment_id, :teacher_id, :student_id)
  end
end
