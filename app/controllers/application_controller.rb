class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def current_user
    @current_user ||= Teacher.find(1)
  end

  def render_js_form objects, *params

    locals = {objects: objects}
    if defined?(params) and params.length > 0
      locals = locals.merge *params
    end
    puts locals
    render  partial: 'shared/form_errors',
            locals: locals
  end

end
