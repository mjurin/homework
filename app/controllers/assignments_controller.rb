class AssignmentsController < ApplicationController
  before_filter :set_assignment, only: [ :show]
  before_filter :set_user, only: [ :create, :show]

  def index
    @students = Student.all
  end

  def new
    @assignment = Assignment.new

    3.times { @assignment.questions.build }
  end

  def create
    @assignment = Assignment.new assignment_params
    @assignment.teacher = @user
    respond_to do |format|
      if @assignment.save
        format.js { render_js_form [@assignment] }
        format.html { teachers_path }
      else
        format.js { render_js_form [@assignment] }
        format.html { teachers_path }
      end
    end
  end

  def show
    @comment = Comment.new
    @comments = Comment.where('assignment_id=?', @assignment.id).order('created_at DESC')
  end

  private
    def set_assignment
      @assignment = Assignment.find(params[:assignment_id])
    end

    def set_user
      if params[:is_student]
        @user = Student.find(params[:id])
      else
        @user = Teacher.find(params[:id])
      end
    end

    def assignment_params
      params.require(:assignment).permit(
        :content_link,
        :student_id,
        :short_description,
        questions_attributes: [:description]
      )
    end
end
