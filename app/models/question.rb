class Question < ActiveRecord::Base
  belongs_to :assignment
  has_many :comments
  validates :description, presence: true
end
