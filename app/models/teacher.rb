class Teacher < ActiveRecord::Base
  has_many :assignments
  has_many :comments
end
