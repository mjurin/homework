class Assignment < ActiveRecord::Base
  belongs_to :teacher
  belongs_to :student
  has_many :questions
  has_many :comments
  validates :content_link, presence: true
  validates :short_description, presence: true
  validates_format_of :content_link, :with => /\A(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\.-]*)*\/?\Z/i

  accepts_nested_attributes_for :questions

end
