class Comment < ActiveRecord::Base
  belongs_to :teacher
  belongs_to :student
  belongs_to :assignment
end
