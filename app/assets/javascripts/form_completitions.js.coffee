$(document).on 'ajax:complete', "form#user_form", ->
  if $(@).hasClass "saved"
    $(@).hide()
    $("#vehicleOwner").show()
    setTimeout (->
      $("#user_link").fadeIn())
      , 500
    $("#ownerPlaceholder").html $("#user_firstname").val() + " " + $("#user_lastname").val()



$(document).on 'ajax:complete', "form[data-remote=true]", ->
  if $(@).hasClass "saved"
    if $(@).find(".alert-success").size() > 0
      $(@).find(".form-content").hide()
      $(@).find(".alert-success").show()

      if $(@).find(".alert-success.auto-hide-timeout").size() > 0
        f = $(@)
        setTimeout (->
            f.find(".alert-success.auto-hide-timeout").hide()
            f.find(".form-content").show()
          ), 2000
