window.last_submitted_form = null
$(document).on "ajax:beforeSend", "form[data-remote=true]", ->
  window.last_submitted_form = $(@)
  $(@).find(".btn-primary").attr('disabled','disabled')
  $(@).find(".btn-primary").data("normal-state-text", $(@).find(".btn-primary").html())
  $(@).find(".btn-primary").html "Submitting ..."


$(document).on "ajax:complete", "form[data-remote=true]", ->
  if !$(@).find(".btn-primary").data("leave-disabled")
    $(@).find(".btn-primary").removeAttr('disabled')
  $(@).find(".btn-primary").html($(@).find(".btn-primary").data("normal-state-text"))


$(document).on "click", ".post-link-actions", ->
  $(@).data 'post-original-text', $(@).html()
  $(@).attr 'disabled', 'disabled'
  $(@).html($(@).data('post-loading-text'))
  link = $(@)
  $.post $(@).attr("href"), {format: 'js'}, (data) ->
    eval data
    link.html(link.data('post-success-text'))
    setTimeout ( ->
      link.removeAttr('disabled')
      link.html(link.data('post-original-text'))
    ), 3000
  return false
