Rails.application.routes.draw do
  resources :teachers do
    member do
      resources :assignments, param: "assignment_id"
    end
  end

  resources :students do
    member do
      resources :assignments, param: "assignment_id", only: [:show]
    end
  end

  #root to: 'teachers#index', as: :root
  resources :comments, only: [:new, :create]
  root to: 'teachers#index'

end
